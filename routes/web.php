<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//  to view
Route::get('/home', function () {
    return view('home');
});
Route::get('/menu', function(){
    return view('menu');
});
Route::get('/search', function(){
    return view('search');
});
Route::get('/cart', function(){
    return view('cart');
});
Route::get('/admin', function(){
    return view('admin');
});

Route::get('/menus', 'MenuController@index');
Route::post('/menus/create', 'MenuController@create');
Route::get('/menus/{id}/edit', 'MenuController@edit');
Route::post('/menus/{id}/update', 'MenuController@update');
Route::get('/menus/{id}/delete', 'MenuController@delete');

Route::get('/Item', 'CartAdminController@index');
Route::post('/Item/create', 'CartAdminController@create');
Route::get('/Item/{id}/edit', 'CartAdminController@edit');
Route::post('/Item/{id}/update', 'CartAdminController@update');
Route::get('/Item/{id}/delete', 'CartAdminController@delete');