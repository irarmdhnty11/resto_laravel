<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\spesialController;
use App\Http\Controllers\comboController;
use App\Http\Controllers\coffeController;
use App\Http\Controllers\cartController;
use App\Http\Controllers\searchController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/getSpesial', [spesialController::class, 'index']);
Route::get('/getCombo', [comboController::class, 'index']);
Route::get('/getCoffe', [coffeController::class, 'index']);
Route::get('/getCart', [cartController::class, 'index']);
Route::get('/getSearch', [searchController::class, 'index']);

