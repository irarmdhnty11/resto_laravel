<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <title>IFC</title>
    <link rel="icon" href="{{asset('assets/img/logo2.png')}}" type="img/x-icon">
  </head>
  <body>
    

    <!--navbar-->
    @include('layouts.header');
    
      <!--carousel-->
      <div class="col-md-15">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active bg-danger"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1" class="bg-danger"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2" class="bg-danger"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3" class="bg-danger"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4" class="bg-danger"></li>
        </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="{{asset('assets/img/slide2.png')}}" class="d-block w-100" alt="slide1" height="537">
            </div>
            <div class="carousel-item">
              <img src="{{asset('assets/img/slide3.png')}}" class="d-block w-100" alt="slide2" height="537">
            </div>
            <div class="carousel-item">
              <img src="{{asset('assets/img/slide4.png')}}" class="d-block w-100" alt="slide3" height="537">
            </div>
            <div class="carousel-item">
              <img src="{{asset('assets/img/slide5.png')}}" class="d-block w-100" alt="slide4" height="537">
            </div>
            <div class="carousel-item">
              <img src="{{asset('assets/img/slide6.png')}}" class="d-block w-100" alt="slide5" height="537">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

    <!--card-->
      <div class="row row-cols-1 row-cols-md-3" id="card">
          <div class="col mb-4">
            <div class="card  card-spesial">
              <img src="{{asset('assets/img/spesial/spesial1.png')}}" class="card-img-top" alt="Spesial 1">
              <div class="card-body">
                <h5 class="card-title">Winger Combo Spicy&Sour</h5>
                <a class="btn btn-danger">Beli Sekarang</a>
              </div>
            </div>
          </div>
          <div class="col mb-4">
            <div class="card  card-spesial">
              <img src="{{asset('assets/img/spesial/spesial2.png')}}" class="card-img-top" alt="Spesial 1">
              <div class="card-body">
                <h5 class="card-title">Winger Combo Spicy&sour</h5>
                <a class="btn btn-danger">Beli Sekarang</a>
              </div>
            </div>
          </div>
          <div class="col mb-4">
            <div class="card  card-spesial">
              <img src="{{asset('assets/img/spesial/spesial3.png')}}" class="card-img-top" alt="Spesial 1">
              <div class="card-body">
                <h5 class="card-title">Fun Fries Spicy&Sour</h5>
                <a class="btn btn-danger">Beli Sekarang</a>
              </div>
            </div>
          </div>
          <div class="col mb-4">
            <div class="card  card-spesial">
              <img src="{{asset('assets/img/spesial/spesial4.png')}}" class="card-img-top" alt="Spesial 1">
              <div class="card-body">
                <h5 class="card-title">Chicken Strips Spicy&Sour</h5>
                <a class="btn btn-danger">Beli Sekarang</a>
              </div>
            </div>
          </div>
        </div>

    <!--footer-->
    @include('layouts.footer')


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>