   <!--navbar-->
  <nav class="navbar navbar-expand-lg px-4 navbar navbar-expand-lg navbar-light bg-danger fixed-top">
      <a class="navbar-brand" href="/home">
        <img src="{{asset('assets/img/your-logo.png')}}" width="100px" height="50px">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNav">
        <span class="toggler-icon"><i class="fas fa-bars"></i></span>
      </button>
      <div class="collapse navbar-collapse" id="myNav">
        <ul class="navbar-nav mx-auto text-capitalize">
          <li class="nav-item active dropdown">
            <a class="nav-link dropdown-toggle" href="layanan.html" id="navbarDropdownMenuLink" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Layanan</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Drive Thru</a>
              <a class="dropdown-item" href="#">Catering</a>
              <a class="dropdown-item" href="#">IFC Coffe</a>
              <a class="dropdown-item" href="#">Breakfast</a>
            </div>
           </li>
          <li class="nav-item active">
            <a class="nav-link" href="/menu">Menu</a>
          </li>
          <li class="nav-item active dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kids</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">BIRTHDAY THEMATIC</a>
              <a class="dropdown-item" href="#">BIRTHDAY FUNTASTIC</a>
              <a class="dropdown-item" href="#">OTHERS PARTY</a>
            </div>
          </li>
        </ul>
        <div class="nav-info-items d-none d-lg-flex ">
         </div>
      </div>
      <form class="form-inline my-2 my-lg-0">
       <a href="/search">
        <input class="form-control mr-sm-2" type="search" placeholder="cari disini" aria-label="Search" id="search-input">
       </a>
      </form>
      <a href="/cart">
        <img src="{{asset('assets/img/cart.png')}}" type="img/x-icon" id="cart">
      </a>  
      <li class="nav-item px-6 d-flex">
       <a href="/admin">
          <img src="{{ asset('assets/img/admin.png') }}" alt="cart-icon" class="admin"/>
       </a>
      </li>
  </nav>
