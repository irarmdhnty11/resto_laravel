<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin</title>
    <link rel="icon" href="{{asset('assets/img/logo2.png')}}" type="img/x-icon">
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
      integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
      crossorigin="anonymous"
    />
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />
    <style>
      .container-admin{
        margin-top: 250px;
      }
      .menu{
        margin-top: 200px;
      }
      .btn-admin{
        margin-bottom: 30px
      }
    </style>
</head>
<body>
    <a class="back" href="/home"><img src="assets/img/back.png" alt="arrow back" id="home"></a>
    <div class="container-admin container text-center">
        <h1>Admin</h1>
        <br/>
        <button type="button" class="btn-admin btn btn-danger btn-lg btn-block text-light"><a href="/menus" class="menu text-decoration-none text-reset"><h3>Menu</h3></a></button>
        <button type="button" class="btn-admin btn btn-danger btn-lg btn-block text-light"><a href="/Item" class="menu text-decoration-none text-reset"><h3>Shopping Cart</h3></a></button>
    </div>
</body>
</html>