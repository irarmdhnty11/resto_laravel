<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <title>IFC</title>
    <link rel="icon" href="{{asset('assets/img/logo2.png')}}" type="img/x-icon">
  </head>
  

<body>
  



  <!--navbar-->
  <nav class="navbar navbar-expand-lg px-4 navbar navbar-expand-lg navbar-light bg-danger fixed-top">
      <a class="navbar-brand" href="/home">
        <img src="{{asset('assets/img/your-logo.png')}}" width="100px" height="50px">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNav">
        <span class="toggler-icon"><i class="fas fa-bars"></i></span>
      </button>
      <div class="collapse navbar-collapse" id="myNav">
        <ul class="navbar-nav mx-auto text-capitalize">
          <li class="nav-item active dropdown">
            <a class="nav-link dropdown-toggle" href="layanan.html" id="navbarDropdownMenuLink" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Layanan</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Drive Thru</a>
              <a class="dropdown-item" href="#">Catering</a>
              <a class="dropdown-item" href="#">IFC Coffe</a>
              <a class="dropdown-item" href="#">Breakfast</a>
            </div>
           </li>
          <li class="nav-item active">
            <a class="nav-link" href="/menu">Menu</a>
          </li>
          <li class="nav-item active dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kids</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">BIRTHDAY THEMATIC</a>
              <a class="dropdown-item" href="#">BIRTHDAY FUNTASTIC</a>
              <a class="dropdown-item" href="#">OTHERS PARTY</a>
            </div>
          </li>
        </ul>
        <div class="nav-info-items d-none d-lg-flex ">
         </div>
      </div>
      <a href="/cart">
        <img src="{{asset('assets/img/cart.png')}}" type="img/x-icon" id="cart">
      </a>  
      <li class="nav-item px-6 d-flex">
       <a href="/admin">
          <img src="{{ asset('assets/img/admin.png') }}" alt="cart-icon" class="admin"/>
       </a>
      </li>
  </nav>



   

    <!--card-->
    <div class="search-container">
        <center><h1 id="search" style="margin-top: 80px;"> Search Menu </h1></center>
        <div class="search-input">
            <center><input id="search-input" type="text" placehaolder="Search Your Menu"/></center>
        </div>
    </div>

    <div class="search-menu" id="back">
        <div class="row row-cols-md-4 card-searchMenu d-flex justify-content-center">
            <div class="col-6 col-md-3 col-search">
              <!-- <div class="card card-searchMenu-item">
                        <img src="Link-gambar" class="card-img-top img-fluid searchMenu-img mx-auto d-block" alt="">
                        <div class="card-body">
                            <h5 class="card-title-searchMenu">nama</h5>
                            <h5 class="card-title-searchMenu">harga</h5>
                        </div>
                    </div>
              -->
            </div>
        </div>
    </div>
    <br>
    <br>

    <!--Fotter-->
    <!--footer-->
    @include('layouts.footer')

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('assets/js/search.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
      integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>