
//spesial//
const spesial = () => {
    fetch('api/getSpesial')
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Something went wrong on api server!');
            }
        })
        .then(data => {
            console.log(data)
            let cardspesial = "";
            data.sort((a, b) => (a.nama > b.nama ? 1 : b.nama > a.nama ? -1 : 0))
            .sort((a, b) => (a.harga > b.harga ? 1 : b.harga > a.harga ? -1 : 0))
            .map(menu => {
                cardspesial += `<div class="col mb-4">
                                    <div class="card  card-spesial">
                                        <img src="${menu.gambar}" class="card-img-top" alt="Poster 1">
                                    <div class="card-body">
                                        <h5 class="card-title">${menu.nama}</h5>
                                        <a class="btn btn-danger">${menu.harga}</a>
                                        <button class="btn btn-danger">
                                            <img src="assets/img/cart.png" type="img/x-icon" id="cart-1"/>
                                        </button>
                                    </div>
                                    </div>
                                </div>`;
            });

            const spesial = document.querySelector("#spesial");
            spesial.innerHTML = cardspesial;

        })
};
spesial();


//combo//
const combo = () => {
    fetch('api/getCombo')
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Something went wrong on api server!');
            }
        })
        .then(data => {
            console.log(data)
            let cardcombo = "";
            data.sort((a, b) => (a.nama > b.nama ? 1 : b.nama > a.nama ? -1 : 0))
            .sort((a, b) => (a.harga > b.harga ? 1 : b.harga > a.harga ? -1 : 0))
            .map(menu => {
                cardcombo += `<div class="col mb-4">
                                    <div class="card  card-combo">
                                        <img src="${menu.gambar}" class="card-img-top" alt="Poster 1">
                                    <div class="card-body">
                                        <h5 class="card-title">${menu.nama}</h5>
                                        <a class="btn btn-danger">${menu.harga}</a>
                                        <button class="btn btn-danger">
                                            <img src="assets/img/cart.png" type="img/x-icon" id="cart-1"/>
                                        </button>
                                    </div>
                                    </div>
                                </div>`;
            });

            const combo = document.querySelector("#combo");
            combo.innerHTML = cardcombo;
            
        }).catch(error => {
            console.error(error);
        });
};
combo();

//coffe//
const coffe = () => {
    fetch('/api/getCoffe')
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Something went wrong on api server!');
            }
        })
        .then(data => {
            console.log(data)
            let cardcoffe = "";
            data.sort((a, b) => (a.nama > b.nama ? 1 : b.nama > a.nama ? -1 : 0))
            .sort((a, b) => (a.harga > b.harga ? 1 : b.harga > a.harga ? -1 : 0))
            .map(menu => {
                cardcoffe += `<div class="col mb-4">
                                    <div class="card  card-coffe">
                                        <img src="${menu.gambar}" class="card-img-top" alt="Poster 1">
                                    <div class="card-body">
                                        <h5 class="card-title">${menu.nama}</h5>
                                        <a class="btn btn-danger">${menu.harga}</a>
                                        <button class="btn btn-danger">
                                            <img src="assets/img/cart.png" type="img/x-icon" id="cart-1"/>
                                        </button>
                                    </div>
                                    </div>
                                </div>`;
            });

            const coffe = document.querySelector("#coffe");
            coffe.innerHTML = cardcoffe;

        }).catch(error => {
            console.error(error);
        });
};
coffe(); 