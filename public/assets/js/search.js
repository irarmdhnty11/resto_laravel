const search = document.querySelector("#search-input");
const cardContainer = document.querySelector(".card-searchMenu");

const searchMenu = async (searchText) => {
    try {
        const res = await fetch("api/getSearch");
        const menus = await res.json();

        let matches = menus.filter((menu) => {
            const regex = new RegExp(`^${searchText}`,`gi`);
            return menu.nama.match(regex);
        });

        if (searchText.lenght === 0){
            matches = [];
        }
        outputHTML(matches);
    } catch (eror) {
        console.eror(eror);
    }
};

const outputHTML = (matches) => {
    let html = "";
        matches.sort((a, b) => (a.nama > b.nama ? 1 : b.nama > a.nama ? -1 : 0))
        .sort((a, b) => (a.harga > b.harga ? 1 : b.harga > a.harga ? -1 : 0))
        .map(match => {
            html += `<div class="col mb-4">
                        <div class="card card-searchMenu-item">
                        <img src="${match.gambar}" class="card-img-top img-fluid searchMenu-img mx-auto d-block" alt="">
                        <div class="card-body">
                            <h5 class="card-title-searchMenu">${match.nama}</h5>
                            <h5 class="card-title-searchMenu">${match.harga}</h5>
                            <button class="btn btn-danger">
                                <img src="assets/img/cart.png" type="img/x-icon" id="cart-1"/>
                            </button>
                        </div>
                    </div>
                    </div>`
        });
    cardContainer.innerHTML = html;
};
search.addEventListener("input", () => searchMenu(search.value));

