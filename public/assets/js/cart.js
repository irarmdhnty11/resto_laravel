const cart = () => {
  var sum = 0;
  const total = document.querySelector(".card");
  fetch('api/getCart')
  .then(response => {
      if (response.status === 200) {
          return response.json();
      } else {
          throw new Error('Something went wrong on api server!');
      }
  })
  .then((data) => {
      let menu = "";
      data.forEach((pesan) => {
          menu += `<div>
          <div class="card-cart cart mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img src="${pesan.gambar}" class="card-img" alt="..." id="pic">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">${pesan.nama}</h5>
              <h4>${pesan.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</h4>
              <div class="input-field">
                 <a class="btn btn-danger">+</a>
                 <input
                   <id="jumlah"
                   name="jumlah"
                   type="number"
                  value="1"
                  min="0"
                   <a class="btn btn-danger">
                   <a class="btn btn-danger">-</a>
              </div>
            </div>
          </div>
        </div>
      </div>`;
          
      });
      const section = document.querySelector(".total");
      section.innerHTML = menu;
      sum = data.reduce((accumulator, curentvalue) => {
        return accumulator + parseInt(curentvalue.harga);
      }, 0);
    total.innerHTML = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  });
};
cart();

