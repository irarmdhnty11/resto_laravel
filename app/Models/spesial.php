<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class spesial extends Model
{
    use HasFactory;
    protected $table = 'spesial';
    protected $fillable = ['gambar', 'nama', 'harga'];
}
