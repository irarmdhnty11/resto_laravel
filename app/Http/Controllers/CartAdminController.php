<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cart;

class CartAdminController extends Controller
{
    public function index(Request $request) 
    {
        if ($request->has('search')) {
            $carts = cart::where('nama', '%'.$request->search.'%')->get();    
        } else {
            $carts = cart::all();
        }

        return view('Admin.index', ['carts'=>$carts]);
    }

    public function create(Request $request)
    {
        $carts = cart::create([
            "gambar" => $request->gambar,
            "nama" => $request->nama,
            "harga" => $request->harga,
        ]);

        return redirect('/Item')->with('success', 'Data Berhasil Diinput');
    }

    public function edit($id) 
    {
        $carts = Cart::find($id);
        return view('/admin/edit', ['carts'=>$carts]);
    }

    public function update(Request $request, $id) 
    {
        $carts = cart::where('id',$id)->update([
            "gambar" => $request->gambar,
            "nama" => $request->nama,
            "harga" => $request->harga,
        ]);

        return redirect('/Item')->with('success', 'Data Berhasil Diupdate');
    }

    public function delete($id) {
        $carts = cart::find($id);
        $carts->delete();

        return redirect('/Item')->with('success', 'Data Berhasil Dihapus');
    }
}
