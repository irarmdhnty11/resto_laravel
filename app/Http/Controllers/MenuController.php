<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\search;

class MenuController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $menu = Menu::where('nama', '%'.$request->search.'%')->get();
        } else {
            $menus = search::all();
        }
        
        return view('crud.index', ['menus'=>$menus]);
    }

    public function create(Request $request)
    {
        $menus = search::create([
            "gambar" => $request->gambar,
            "nama" => $request->nama,
            "harga" => $request->harga,
        ]);

        return redirect('/menus')->with('success', 'Data Berhasil Diinput');
    }

    public function edit($id)
    {
        $menus = search::find($id);
        return view('/crud/edit', ['menus'=>$menus]);
    }

    public function update(Request $request, $id)
    {
        $menus = search::where('id',$id)->update([
            "gambar" => $request->gambar,
            "nama" => $request->nama,
            "harga" => $request->harga,
        ]);

        return redirect('/menus')->with('success', 'Data Berhasil diupdate');
    }

    public function delete($id)
    {
        $menus = search::find($id);
        $menus->delete();

        return redirect('/menus')->with('success', 'Data Berhasil dihapus');
    }
}
